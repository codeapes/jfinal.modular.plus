package plus.modular.jfinal.web.service;

import com.jfinal.kit.Result;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

import java.util.Map;

public abstract class BaseService {

    public Result paginate(String listSqlTemplate, String countSqlTemplate, Map data){
        Result result = new Result();
        SqlPara sqlPara = Db.getSqlPara(listSqlTemplate,data);
        result.set("data",Db.find(sqlPara));
        sqlPara = Db.getSqlPara(countSqlTemplate, data);
        result.set("total",Db.queryLong(sqlPara.getSql(),sqlPara.getPara()));
        return result;
    }
}
