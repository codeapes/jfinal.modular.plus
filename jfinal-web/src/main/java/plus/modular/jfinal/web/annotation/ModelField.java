package plus.modular.jfinal.web.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ModelField {

    /**
     * 列名
     * <p>数据库中的字段名称，通常是下划线分隔</p>
     * @return
     */
    String field() default "";
    /**
     * 参数的（中文）名称
     * @return
     */
    String text() default "";

    /**
     * 日期格式转换格式
     * @return
     */
    String dateFormat() default "yyyy-MM-dd HH:mm:ss";
}
