package plus.modular.jfinal.web.interceptor;


import cn.hutool.core.exceptions.ExceptionUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import plus.modular.jfinal.web.controller.BaseController;

public class ExceptionInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		BaseController controller = (BaseController)inv.getController();
        try{
        	inv.invoke();
        }catch(Exception e){
            System.out.println(ExceptionUtil.getMessage(e));
            e.printStackTrace();
        }
	}

	
}
