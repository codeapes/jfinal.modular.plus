package com.jfinal.plugin.log4j2;

import com.jfinal.log.ILogFactory;
import com.jfinal.log.Log4j2Factory;
import com.jfinal.plugin.IPlugin;
import org.apache.logging.log4j.core.config.Configurator;

public class Log4j2Plugin implements IPlugin {

    private String configPath = "classpath:config/log4j2.xml";

    private ILogFactory logFactory;

    public Log4j2Plugin(){}

    public Log4j2Plugin(String configPath){
        this.configPath = configPath;
    }

    public boolean start() {
        Configurator.initialize("Log4j2", this.configPath);
        logFactory = new Log4j2Factory();
        if(logFactory!=null){
            logFactory.getLog(getClass().getName()).info("[Log4j2Plugin] - 插件初始化成功");
            return true;
        }else{
            logFactory.getLog(getClass().getName()).info("[Log4j2Plugin] - 插件初始化失败");
            return false;
        }
    }

    public boolean stop() {
        return false;
    }

    public ILogFactory getLogFactory(){
        if(logFactory == null){
            start();
        }
        return this.logFactory;
    }


}
